using System;
using Xunit;

using mesozoic_console;

namespace mesozoic_test
{
    public class DinausorTest
    {
        [Fact]
        public void TestDinosaurConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

            Assert.Equal("Louis", louis.getName());
            Assert.Equal("Stegausaurus", louis.getSpecie());
            Assert.Equal(12, louis.getAge());
        }
        [Fact]
        public void TestDinosaurRoar()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal("Grrr", louis.roar());
        }
        [Fact]
        public void TestDinosaurSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.sayHello());
        }
        [Fact]
        public void TestDinosaurSetAge()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            louis.setAge(13);
            Assert.Equal(13, louis.getAge());
        }
        [Fact]
        public void TestDinosaurSetName()
        {
            Dinosaur louis = new Dinosaur("Luois", "Stegausaurus", 12);
            louis.setName("Louis");
            Assert.Equal("Louis", louis.getName());
        }
        [Fact]
        public void TestDinosaurSetSpecie()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausau", 12);
            louis.setSpecie("Stegausaurus");
            Assert.Equal("Stegausaurus", louis.getSpecie());
        }
        [Fact]
        public void TestDinosaurHug()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Assert.Equal("Je suis Louis et je fais un calin à Nessie.", louis.hug(nessie));
        }
    }
}
