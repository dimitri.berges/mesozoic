using System;

namespace mesozoic_console
{
    public class Dinosaur
    {
        private string name;
        private string specie;
        private int age;

        public Dinosaur(string name, string specie, int age)
        {
            this.name = name;
            this.specie = specie;
            if (age < 0) this.age = -age;
            else this.age = age;
        }
        public void setName(string newName) {
            this.name = newName;
        }
        public void setSpecie(string newSpecie) {
            this.specie = newSpecie;
        }
        public void setAge(int newAge) {
            if (newAge < 0) this.age = -newAge;
            else this.age = newAge;
        }
        public string getName() {
            return this.name;
        }
        public string getSpecie() {
            return this.specie;
        }
        public int getAge() {
            return this.age;
        }
        public string sayHello()
        {
            return String.Format("Je suis {0} le {1}, j'ai {2} ans.", this.name, this.specie, this.age);
        }
        public string roar()
        {
            return "Grrr";
        }
        public string hug(Dinosaur otherDinosaur) {
            return String.Format("Je suis {0} et je fais un calin à {1}.", this.name, otherDinosaur.getName());
        }
    }
}